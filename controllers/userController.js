// import dependencies and module files
const User= require("../models/User");
const bcrypt= require("bcrypt");
const auth= require("../auth");
const Product= require("../models/Product");

// export functions

	// check duplicate email
	module.exports.checkEmailExists= (reqBody)=> {
		return User.find({email: reqBody.email}).then(result =>{
			if (result.length > 0){
				return true;

			} else {
				return false;
			}
		})
	};

	//register user
	module.exports.registerUser= (reqBody)=> {
		let newUser= new User({
			firstName: reqBody.firstName,
			lastName: reqBody.lastName,
			email: reqBody.email,
			mobileNo: reqBody.mobileNo,
			password: bcrypt.hashSync(reqBody.password, 10)
		});

		return newUser.save().then((user, error)=> {
			if(error) {
				return false
			} else{
				return true
			}
		})
	};


	//user login authentication
	module.exports.loginUser= (reqBody)=> {
		return User.findOne({email: reqBody.email}).then(result=> {
			if(result == null){
				return false
			} else{
					const isPasswordCorrect= bcrypt.compareSync(reqBody.password, result.password);
					if(isPasswordCorrect){
						return {access: auth.createAccessToken(result)}
					} else {
						return false
					}
			}
		})
	};

	// non-admin user checkout

	module.exports.checkout= async(data)=> {
		if(!data.isAdmin){
			let isUserUpdated= await User.findById(data.userId).then(user=>{
				user.orderedProducts.push({
					products: {
						productId: data.productId,
						productName: data.productName,
						quantity: data.quantity
					},

					totalAmount: data.totalAmount
				})

				return user.save().then((user, error)=> {

					if(error){
						return false
					} else{
						return true
					};
				});
			});

			let isProductUpdated= await Product.findById(data.productId).then(product=> {

				product.userOrders.push({userId: data.userId})
				return product.save().then((product, error)=> {
					if(error){
						return false
					} else{
						return true
					};
				});
			});

			if(isUserUpdated && isProductUpdated){
				/* return 'User checkout successful'; */
				return true
			} else{
				return false
			};
		};

		let message= Promise.resolve({
			message: "For non-admin users only!"
		})

		return message.then((value)=> {
			return value
		})
	}

	

	/*// retrieve user profile [auth users]
	module.exports.getProfile= (reqParams, data)=> {
	return User.findById(reqParams.id, data.id).then(result=> {
		if(reqParams.id == data.userId){


				result.password= ""
				return result;

			} 

			let message= Promise.resolve({
				message: "You cannot view other users' details."
			})

			return message.then((value)=> {
				return value
			})
		
		
		})
	}*/

	// retrieve user details [auth users]
	module.exports.getProfile= (data)=> {
	return User.findById(data.userId).then(result=> {
			if(result == null){
				return false;

			} else{

				result.password= "";
				return result;

			};
			
		});
	};