// import dependencies and module files
const Product= require("../models/Product");

// export functions
	
	// add product [admin only]
	module.exports.addProduct= (data)=> {
		if(data.isAdmin){
			let newProduct= new Product({
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			});

			return newProduct.save().then((product, error)=> {
				if(error){
					return false
				} else{
					return true
				}
			})
		}

		let message= Promise.resolve({
			message: "User must be an Admin to access this!"
		})

		return message.then((value)=> {
			return value
		})
	};

	// retrieve all products
	module.exports.getAllProducts= ()=> {
		return Product.find({}).then(result=> {
			return result
		})
	};

	// retrieve all 'active' products
	module.exports.getAllActive= ()=> {
		return Product.find({isActive: true}).then(result=> {
			return result
		});
	};

	// retrieve single product - id url endpoint
	module.exports.getProduct= (reqParams)=> {
		return Product.findById(reqParams.productId).then(result=> {
			return result
		});
	} ;

	// update product [admin only]
	module.exports.updateProduct= (reqParams, data)=> {
		if(data.isAdmin){

			let updatedProduct= {
				name: data.product.name,
				description: data.product.description,
				price: data.product.price
			};

			return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error)=> {

				if(error){
					return false
				} else{

					return true

					/* let message= `Successfully updated product Id- "${reqParams.productId}`

					return message */
				}
			})
		}

		let message= Promise.resolve({
		message: "User must be an Admin to access this!"
		})

		return message.then((value)=> {
			return value;
		})
	};

	// archive product [admin only]
	module.exports.archiveProduct= (reqParams, data)=> {
		if(data.isAdmin){
			let updateActiveField= {
				isActive: false
			};

			return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error)=> {
				if(error){
					return false
				} else{
					return true
					
				};
			});
		};

		let message= Promise.resolve({
		message: "User must be an Admin to access this!"
		});

		return message.then((value)=> {
			return value;
		});
	};

	// activate product [admin only]
	module.exports.activateProduct= (reqParams, data)=> {
		if(data.isAdmin){
			let updateActiveField= {
				isActive: true
			};

			return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((product, error)=> {
				if(error){
					return false
				} else{
					 return true
					
				};
			});
		};

		let message= Promise.resolve({
		message: "User must be an Admin to access this!"
		});

		return message.then((value)=> {
			return value;
		});
	};

