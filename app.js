// port localhost
const port= 4009;

// setup dependencies
const express= require("express");
const mongoose= require("mongoose");
const cors= require("cors");

// setup express-cors for app use
const app= express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// setup main routes

	// users
		const userRoutes= require("./routes/userRoutes") 
		app.use("/users", userRoutes);

	// products
		const productRoutes= require("./routes/productRoutes")
		app.use("/products", productRoutes);


// setup mongodb connection
mongoose.connect("mongodb+srv://tejuco-voren:Cheersbr021@zuitt-bootcamp.muegp8u.mongodb.net/s42-s46?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', ()=> console.log('Now connected to MongoDb Atlas'));

// listen port localhost
app.listen(port, ()=> console.log(`API is now online on port ${port}`));