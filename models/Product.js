// import dependency
const mongoose= require("mongoose");

// model schema
const productSchema= new mongoose.Schema({
	name: {
		type: String,
		required: [true, "Course is required"]
	},
	description: {
		type: String,
		required: [true, "Description is required"]
	},

	price: {
		type: Number,
		required: [true, "Price is required"]
	},

	
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		default: new Date()
	},
	
	userOrders: [{
				userId: {
					type: String,
					required: [true, "User ID is required"]
				}, 
				orderId: {
					type: String,
					required: false
				}
		}]
});


// export model schema
module.exports= mongoose.model("Product", productSchema);