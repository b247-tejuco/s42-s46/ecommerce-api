// import dependencies and module files
const userController= require("../controllers/userController");
const auth= require("../auth");
const express= require("express");


// ROUTES
const router= express.Router();
	
	// check email
	router.post("/checkEmail", (req, res)=> {
		userController.checkEmailExists(req.body).then(resultFromController=> res.send(resultFromController));
	});

	// register user
	router.post("/register", (req, res)=> {
		userController.registerUser(req.body).then(resultFromController=> res.send(resultFromController));
	});

	// user login authentication
	router.post("/login", (req, res)=> {
		userController.loginUser(req.body).then(resultFromController=> res.send(resultFromController));
	});

	// non-admin user checkout
	router.post("/checkout", auth.verify, (req, res)=> {
		let data= {
			userId: auth.decode(req.headers.authorization).id,
			isAdmin: auth.decode(req.headers.authorization).isAdmin,
			productId: req.body.productId,
			productName: req.body.productName,
			quantity: req.body.quantity,
			totalAmount: req.body.totalAmount
		}

		userController.checkout(data).then(resultFromController=> res.send(resultFromController));
	});
	
	

	/*// retrieve user profile [auth users]
	router.get("/:id/userdetails", auth.verify, (req, res)=> {

	const userData= auth.decode(req.headers.authorization);
	let data= {userId: userData.id}

	userController.getProfile(req.params, data).then(resultFromController=> res.send(resultFromController));
	})*/

	// retrieve user profile [auth users]
	router.get("/userdetails", auth.verify, (req, res)=> {

	const userData= auth.decode(req.headers.authorization);
	

	userController.getProfile({userId: userData.id}).then(resultFromController=> res.send(resultFromController));
	})



// export routes
module.exports= router;