// import dependencies and modules
const express= require("express");
const productController= require("../controllers/productController");
const auth= require("../auth");

// ROUTES
const router= express.Router();

	// add product [admin only]
	router.post("/", auth.verify, (req, res)=> {

		const data= {
			product: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		productController.addProduct(data).then(resultFromController=> res.send(resultFromController))
	});

	// retrieve all products
	router.get("/all", (req, res)=> {
		productController.getAllProducts().then(resultFromController=> res.send(resultFromController));
	});

	// retrieve all 'active' products
	router.get("/", (req, res)=> {
		productController.getAllActive().then(resultFromController=> res.send(resultFromController));
	});

	// retrieve single product - id url endpoint
	router.get("/:productId", (req, res)=> {
		productController.getProduct(req.params).then(resultFromController=> res.send(resultFromController));
	});

	// update product [admin only]
	router.put("/:productId", auth.verify, (req, res)=> {
		const data= {
			product: req.body,
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		productController.updateProduct(req.params, data).then(resultFromController=> res.send(resultFromController));
	});

	// archive product [admin only]
	router.put("/:productId/archive", auth.verify, (req, res)=> {
		const data= {
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		productController.archiveProduct(req.params, data).then(resultFromController=> res.send(resultFromController));
	});

	// activate product [admin only]
	router.put("/:productId/activate", auth.verify, (req, res)=> {
		const data= {
			isAdmin: auth.decode(req.headers.authorization).isAdmin
		}

		productController.activateProduct(req.params, data).then(resultFromController=> res.send(resultFromController));
	});

	


// export routes
module.exports= router;