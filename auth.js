// import dependencies
const jwt= require("jsonwebtoken");
const secret= "EcommerceAPI";

// export functions
module.exports.createAccessToken= (user)=> {
	const data= {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};

	return jwt.sign(data, secret, {});
};

module.exports.verify= (req, res, next)=> {
	let token= req.headers.authorization;

	if(typeof token !== "undefined"){
		token= token.slice(7, token.length);
		


		return jwt.verify(token, secret, (error, data)=> {
			if(error){
				return res.send({auth: "failed"});
			} else{
				next()
			}
		})
	} else{
		return res.send({auth: "failed"})
	};
};


module.exports.decode= (token)=> {
	if(typeof token !== "undefined"){
		token= token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data)=> {
			if(error){
				return null
			} else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	} else{
		return null
	};
};

/*const isNonAdmin = async (req, res, next) => {

    if (req.user.isAdmin)
        return res.status(401).send({ msg: "Sorry, this is only accessible for non-Admin users" });

    next();
};

const isAdmin = async (req, res, next) => {

    if (!req.user.isAdmin)
        return res.status(401).send({ msg: "User must be an Admin to access this!" });

    next();
};*/